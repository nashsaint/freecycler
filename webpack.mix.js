const { sourceMaps } = require('laravel-mix');
const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/modules/chat.js', 'public/js')
    // .js('resources/js/main.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .copy('resources/css/main.css', 'public/css')
    .copy('resources/imgs', 'public/imgs')
    .copy('resources/fonts', 'public/fonts')
    .copy('resources/demo', 'public/demo')
    .sourceMaps();
