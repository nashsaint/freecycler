<?php

namespace Database\Seeders;

use App\Models\Messages\Thread;
use App\Models\Messages\Message;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ThreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $thread1 = Thread::create([
            'post_id' => 1,
            'from_id' => 2,
            'to_id' => 1
            ]);
            $this->create($thread1, 2);

        $thread2 = Thread::create([
            'post_id' => 1,
            'from_id' => 3,
            'to_id' => 1
        ]);
        $this->create($thread2, 3);
    }

    public function create($thread, $from)
    {
        $date = Carbon::now();
        $fromArr = [1, $from];

        for ($i=0; $i < 10; $i++) {
            Message::factory()
                ->times(1)
                ->create([
                    'thread_id' => $thread->id,
                    'from_id' => $fromArr[rand(0, 1)],
                    'created_at' => $date->addMinute($i)
                ]);
        }
    }
}
