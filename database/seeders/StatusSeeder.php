<?php

namespace Database\Seeders;

use App\Models\Posts\PostStatus;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostStatus::insert($this->list());
    }

    public function list()
    {
        $now = Carbon::now();

        return [
            [
                'name' => 'draft',
                'class' => 'bg-gray-dark text-white',
                'created_at' => $now,
            ],
            [
                'name' => 'published',
                'class' => 'bg-green-light text-white',
                'created_at' => $now,
            ],
            [
                'name' => 'taken',
                'class' => 'bg-gray text-white',
                'created_at' => $now,
            ],
            [
                'name' => 'deleted',
                'class' => 'bg-red-light text-white',
                'created_at' => $now,
            ],
            [
                'name' => 'archived',
                'class' => 'bg-gray-lighter text-white',
                'created_at' => $now,
            ],
        ];
    }
}
