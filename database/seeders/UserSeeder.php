<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Nash',
            'lastname' => 'delos Santos',
            'email' => 'nashsaint@gmail.com',
            'password' => Hash::make('password')
        ]);

        User::factory()
            ->times(10)
            ->create();
    }
}
