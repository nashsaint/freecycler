<?php

namespace Database\Factories\Posts;

use App\Models\Posts\Post;
use App\Models\Posts\PostStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $status = PostStatus::inRandomOrder()->first();

        return [
            'user_id' => 1,
            'title' => $this->faker->realText(50),
            'description' => $this->faker->realText(200),
            'status_id' => $status->id
        ];
    }
}
