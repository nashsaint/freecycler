/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/modules/chat.js":
/*!**************************************!*\
  !*** ./resources/js/modules/chat.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// const { default: Axios } = require(\"axios\");\n// var element = $('.floating-chat');\n// var myStorage = localStorage;\n// if (!myStorage.getItem('chatID')) {\n//     myStorage.setItem('chatID', createUUID());\n// }\n// setTimeout(function() {\n//     element.addClass('enter');\n// }, 1000);\n// // element.click(openElement);\n// function openElement() {\n//     var messages = element.find('.messages');\n//     var textInput = element.find('.text-box');\n//     element.find('>i').hide();\n//     element.addClass('expand');\n//     element.find('.chat').addClass('enter');\n//     var strLength = textInput.val().length * 2;\n//     textInput.keydown(onMetaAndEnter).prop(\"disabled\", false).focus();\n//     element.off('click', openElement);\n//     element.find('.header button').click(closeElement);\n//     element.find('#sendMessage').click(sendNewMessage);\n//     messages.scrollTop(messages.prop(\"scrollHeight\"));\n// }\n// function closeElement() {\n//     element.find('.chat').removeClass('enter').hide();\n//     element.find('>i').show();\n//     element.removeClass('expand');\n//     element.find('.header button').off('click', closeElement);\n//     element.find('#sendMessage').off('click', sendNewMessage);\n//     element.find('.text-box').off('keydown', onMetaAndEnter).prop(\"disabled\", true).blur();\n//     setTimeout(function() {\n//         element.find('.chat').removeClass('enter').show()\n//         element.click(openElement);\n//     }, 500);\n// }\n// function createUUID() {\n//     // http://www.ietf.org/rfc/rfc4122.txt\n//     var s = [];\n//     var hexDigits = \"0123456789abcdef\";\n//     for (var i = 0; i < 36; i++) {\n//         s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);\n//     }\n//     s[14] = \"4\"; // bits 12-15 of the time_hi_and_version field to 0010\n//     s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01\n//     s[8] = s[13] = s[18] = s[23] = \"-\";\n//     var uuid = s.join(\"\");\n//     return uuid;\n// }\n// function sendNewMessage() {\n//     var userInput = $('.text-box');\n//     var newMessage = userInput.html().replace(/\\<div\\>|\\<br.*?\\>/ig, '\\n').replace(/\\<\\/div\\>/g, '').trim().replace(/\\n/g, '<br>');\n//     if (!newMessage) return;\n//     var messagesContainer = $('.messages');\n//     Echo.channel('chat').listen('MessageCreated', e => {\n//         console.log('xxx')\n//     })\n//     Axios.post('/admin/messages', {message: newMessage})\n//         .then(R => {\n//             messagesContainer.append([\n//                 '<li class=\"self\">',\n//                 newMessage,\n//                 '</li>'\n//             ].join(''));\n//             // clean out old message\n//             userInput.html('');\n//             // focus on input\n//             userInput.focus();\n//             messagesContainer.finish().animate({\n//                 scrollTop: messagesContainer.prop(\"scrollHeight\")\n//             }, 250);\n//         })\n//         .catch(R => {\n//             console.log(R)\n//         })\n// }\n// function onMetaAndEnter(event) {\n//     if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {\n//         sendNewMessage();\n//     }\n// }//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9jaGF0LmpzP2UzZjMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiIuL3Jlc291cmNlcy9qcy9tb2R1bGVzL2NoYXQuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBjb25zdCB7IGRlZmF1bHQ6IEF4aW9zIH0gPSByZXF1aXJlKFwiYXhpb3NcIik7XG5cbi8vIHZhciBlbGVtZW50ID0gJCgnLmZsb2F0aW5nLWNoYXQnKTtcbi8vIHZhciBteVN0b3JhZ2UgPSBsb2NhbFN0b3JhZ2U7XG5cbi8vIGlmICghbXlTdG9yYWdlLmdldEl0ZW0oJ2NoYXRJRCcpKSB7XG4vLyAgICAgbXlTdG9yYWdlLnNldEl0ZW0oJ2NoYXRJRCcsIGNyZWF0ZVVVSUQoKSk7XG4vLyB9XG5cbi8vIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4vLyAgICAgZWxlbWVudC5hZGRDbGFzcygnZW50ZXInKTtcbi8vIH0sIDEwMDApO1xuXG4vLyAvLyBlbGVtZW50LmNsaWNrKG9wZW5FbGVtZW50KTtcblxuLy8gZnVuY3Rpb24gb3BlbkVsZW1lbnQoKSB7XG4vLyAgICAgdmFyIG1lc3NhZ2VzID0gZWxlbWVudC5maW5kKCcubWVzc2FnZXMnKTtcbi8vICAgICB2YXIgdGV4dElucHV0ID0gZWxlbWVudC5maW5kKCcudGV4dC1ib3gnKTtcbi8vICAgICBlbGVtZW50LmZpbmQoJz5pJykuaGlkZSgpO1xuLy8gICAgIGVsZW1lbnQuYWRkQ2xhc3MoJ2V4cGFuZCcpO1xuLy8gICAgIGVsZW1lbnQuZmluZCgnLmNoYXQnKS5hZGRDbGFzcygnZW50ZXInKTtcbi8vICAgICB2YXIgc3RyTGVuZ3RoID0gdGV4dElucHV0LnZhbCgpLmxlbmd0aCAqIDI7XG4vLyAgICAgdGV4dElucHV0LmtleWRvd24ob25NZXRhQW5kRW50ZXIpLnByb3AoXCJkaXNhYmxlZFwiLCBmYWxzZSkuZm9jdXMoKTtcbi8vICAgICBlbGVtZW50Lm9mZignY2xpY2snLCBvcGVuRWxlbWVudCk7XG4vLyAgICAgZWxlbWVudC5maW5kKCcuaGVhZGVyIGJ1dHRvbicpLmNsaWNrKGNsb3NlRWxlbWVudCk7XG4vLyAgICAgZWxlbWVudC5maW5kKCcjc2VuZE1lc3NhZ2UnKS5jbGljayhzZW5kTmV3TWVzc2FnZSk7XG4vLyAgICAgbWVzc2FnZXMuc2Nyb2xsVG9wKG1lc3NhZ2VzLnByb3AoXCJzY3JvbGxIZWlnaHRcIikpO1xuLy8gfVxuXG4vLyBmdW5jdGlvbiBjbG9zZUVsZW1lbnQoKSB7XG4vLyAgICAgZWxlbWVudC5maW5kKCcuY2hhdCcpLnJlbW92ZUNsYXNzKCdlbnRlcicpLmhpZGUoKTtcbi8vICAgICBlbGVtZW50LmZpbmQoJz5pJykuc2hvdygpO1xuLy8gICAgIGVsZW1lbnQucmVtb3ZlQ2xhc3MoJ2V4cGFuZCcpO1xuLy8gICAgIGVsZW1lbnQuZmluZCgnLmhlYWRlciBidXR0b24nKS5vZmYoJ2NsaWNrJywgY2xvc2VFbGVtZW50KTtcbi8vICAgICBlbGVtZW50LmZpbmQoJyNzZW5kTWVzc2FnZScpLm9mZignY2xpY2snLCBzZW5kTmV3TWVzc2FnZSk7XG4vLyAgICAgZWxlbWVudC5maW5kKCcudGV4dC1ib3gnKS5vZmYoJ2tleWRvd24nLCBvbk1ldGFBbmRFbnRlcikucHJvcChcImRpc2FibGVkXCIsIHRydWUpLmJsdXIoKTtcbi8vICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuLy8gICAgICAgICBlbGVtZW50LmZpbmQoJy5jaGF0JykucmVtb3ZlQ2xhc3MoJ2VudGVyJykuc2hvdygpXG4vLyAgICAgICAgIGVsZW1lbnQuY2xpY2sob3BlbkVsZW1lbnQpO1xuLy8gICAgIH0sIDUwMCk7XG4vLyB9XG5cbi8vIGZ1bmN0aW9uIGNyZWF0ZVVVSUQoKSB7XG4vLyAgICAgLy8gaHR0cDovL3d3dy5pZXRmLm9yZy9yZmMvcmZjNDEyMi50eHRcbi8vICAgICB2YXIgcyA9IFtdO1xuLy8gICAgIHZhciBoZXhEaWdpdHMgPSBcIjAxMjM0NTY3ODlhYmNkZWZcIjtcbi8vICAgICBmb3IgKHZhciBpID0gMDsgaSA8IDM2OyBpKyspIHtcbi8vICAgICAgICAgc1tpXSA9IGhleERpZ2l0cy5zdWJzdHIoTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMHgxMCksIDEpO1xuLy8gICAgIH1cbi8vICAgICBzWzE0XSA9IFwiNFwiOyAvLyBiaXRzIDEyLTE1IG9mIHRoZSB0aW1lX2hpX2FuZF92ZXJzaW9uIGZpZWxkIHRvIDAwMTBcbi8vICAgICBzWzE5XSA9IGhleERpZ2l0cy5zdWJzdHIoKHNbMTldICYgMHgzKSB8IDB4OCwgMSk7IC8vIGJpdHMgNi03IG9mIHRoZSBjbG9ja19zZXFfaGlfYW5kX3Jlc2VydmVkIHRvIDAxXG4vLyAgICAgc1s4XSA9IHNbMTNdID0gc1sxOF0gPSBzWzIzXSA9IFwiLVwiO1xuXG4vLyAgICAgdmFyIHV1aWQgPSBzLmpvaW4oXCJcIik7XG4vLyAgICAgcmV0dXJuIHV1aWQ7XG4vLyB9XG5cbi8vIGZ1bmN0aW9uIHNlbmROZXdNZXNzYWdlKCkge1xuLy8gICAgIHZhciB1c2VySW5wdXQgPSAkKCcudGV4dC1ib3gnKTtcbi8vICAgICB2YXIgbmV3TWVzc2FnZSA9IHVzZXJJbnB1dC5odG1sKCkucmVwbGFjZSgvXFw8ZGl2XFw+fFxcPGJyLio/XFw+L2lnLCAnXFxuJykucmVwbGFjZSgvXFw8XFwvZGl2XFw+L2csICcnKS50cmltKCkucmVwbGFjZSgvXFxuL2csICc8YnI+Jyk7XG5cbi8vICAgICBpZiAoIW5ld01lc3NhZ2UpIHJldHVybjtcblxuLy8gICAgIHZhciBtZXNzYWdlc0NvbnRhaW5lciA9ICQoJy5tZXNzYWdlcycpO1xuXG4vLyAgICAgRWNoby5jaGFubmVsKCdjaGF0JykubGlzdGVuKCdNZXNzYWdlQ3JlYXRlZCcsIGUgPT4ge1xuLy8gICAgICAgICBjb25zb2xlLmxvZygneHh4Jylcbi8vICAgICB9KVxuXG4vLyAgICAgQXhpb3MucG9zdCgnL2FkbWluL21lc3NhZ2VzJywge21lc3NhZ2U6IG5ld01lc3NhZ2V9KVxuLy8gICAgICAgICAudGhlbihSID0+IHtcbi8vICAgICAgICAgICAgIG1lc3NhZ2VzQ29udGFpbmVyLmFwcGVuZChbXG4vLyAgICAgICAgICAgICAgICAgJzxsaSBjbGFzcz1cInNlbGZcIj4nLFxuLy8gICAgICAgICAgICAgICAgIG5ld01lc3NhZ2UsXG4vLyAgICAgICAgICAgICAgICAgJzwvbGk+J1xuLy8gICAgICAgICAgICAgXS5qb2luKCcnKSk7XG5cbi8vICAgICAgICAgICAgIC8vIGNsZWFuIG91dCBvbGQgbWVzc2FnZVxuLy8gICAgICAgICAgICAgdXNlcklucHV0Lmh0bWwoJycpO1xuLy8gICAgICAgICAgICAgLy8gZm9jdXMgb24gaW5wdXRcbi8vICAgICAgICAgICAgIHVzZXJJbnB1dC5mb2N1cygpO1xuXG4vLyAgICAgICAgICAgICBtZXNzYWdlc0NvbnRhaW5lci5maW5pc2goKS5hbmltYXRlKHtcbi8vICAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IG1lc3NhZ2VzQ29udGFpbmVyLnByb3AoXCJzY3JvbGxIZWlnaHRcIilcbi8vICAgICAgICAgICAgIH0sIDI1MCk7XG4vLyAgICAgICAgIH0pXG4vLyAgICAgICAgIC5jYXRjaChSID0+IHtcbi8vICAgICAgICAgICAgIGNvbnNvbGUubG9nKFIpXG4vLyAgICAgICAgIH0pXG4vLyB9XG5cbi8vIGZ1bmN0aW9uIG9uTWV0YUFuZEVudGVyKGV2ZW50KSB7XG4vLyAgICAgaWYgKChldmVudC5tZXRhS2V5IHx8IGV2ZW50LmN0cmxLZXkpICYmIGV2ZW50LmtleUNvZGUgPT0gMTMpIHtcbi8vICAgICAgICAgc2VuZE5ld01lc3NhZ2UoKTtcbi8vICAgICB9XG4vLyB9XG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/modules/chat.js\n");

/***/ }),

/***/ 1:
/*!********************************************!*\
  !*** multi ./resources/js/modules/chat.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nashdelossantos/Site/freecycler/resources/js/modules/chat.js */"./resources/js/modules/chat.js");


/***/ })

/******/ });