@extends('layouts.error')

@section('title')
{{ $exception->getStatusCode() }}
@endsection

@section('error-code')
    Nearly There
@endsection

@section('message')
    <h2>Down For Maintenance</h2>
    <p class="mb-5">We are working hard to get the functionalities and your experience perfect for when we launch the site.  Please visit soon.
      </p>
      <div class="form-group">
        <label class="form-label">Subscribe</label>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="email@address.com">
            <span class="input-group-append">
                <button class="btn btn-primary" type="button">Submit</button>
            </span>
        </div>
    </div>
@endsection
