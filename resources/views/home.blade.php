@extends('layouts.app')

@section('content')
<div class="container">
    <div class="page-header">
        <h2 class="page-title">Recent Items</h2>
    </div>

    <post-list></post-list>

    @endsection
