@extends('layouts.app')

@section('content')
    <post-form :statuses="{{ $statuses }}"></post-form>
@endsection
