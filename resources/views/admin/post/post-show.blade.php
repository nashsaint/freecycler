@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Viewing a post</h2>
                        <div class="card-options">
                            <a class="btn btn-sm btn-outline-primary" href="{{ route('admin.posts.index') }}"><i class="fe fe-arrow-left-circle"></i> return to list</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="item mb-5">
                            <small class="text-muted text-uppercase">Title</small>
                            <div>{{ $post->title }}</div>
                        </div>
                        <div class="item mb-5">
                            <small class="text-muted text-uppercase d-block">Status</small>
                            <span class="badge {{ $post->status->class }}">{{ $post->status->name }}</span>
                        </div>
                        <div class="item mb-5">
                            <small class="text-muted text-uppercase d-block">Description</small>
                            {{ $post->description }}
                        </div>
                    </div>
                    <div class="card-footer justify-content-between d-flex">
                        <form action="{{ route('admin.posts.destroy', ['post' => $post->id]) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="delete">
                            <button class="btn btn-sm btn-danger" type="submit"><i class="fe fe-x"></i> Permanently Delete</button>
                        </form>
                        <a href="#" class="btn btn-sm btn-primary"><i class="fe fe-edit-2"></i> Restore Post</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Images</h2>
                    </div>
                    <div class="card-body">
                        @if (count($post->images) > 0)
                            <div class="row">
                                @foreach ($post->images as $image)
                                    <div class="col-6 col-sm-6 mb-4 post-edit-img">
                                        <img src="https://freecycler.s3.eu-west-2.amazonaws.com/posts/{{image.filename}}" class="img-thumbnail">
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="alert bg-gray-lightest">No Images</div>
                        @endif
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Messages</h2>
                    </div>
                    <div class="card-body">
                        @if (count($post->threads) > 0)
                            <ul class="list-unstyled list-separated">
                                @foreach ($post->threads as $thread)
                                    <li class="list-separated-item">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
                                                <span class="avatar avatar-md d-block" style="background-image: url(/imgs/avatar.png)"></span>
                                            </div>
                                            <div class="col">
                                                <div>
                                                    {{ $thread->from->fullname }}
                                                </div>
                                                <small class="d-block item-except text-sm h-1x"></small>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <div class="alert bg-gray-lightest">No Messages</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
