@extends('layouts.app')

@section('content')
    <post-form :data="{{ $post }}" :statuses="{{ $statuses }}"></post-form>
@endsection
