@extends('layouts.app')

@section('content')
    <div class='container'>
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Post Archive</h2>
                <div class="card-options">
                    <a href="{{ route('admin.posts.create') }}" class="btn btn-sm btn-outline-primary"><i class="fe fe-edit-2"></i> create post</a>
                </div>
            </div>
            <div class="card-body">
                @if (count($posts))
                    <div class="table-responsive">
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Title</th>
                                    <th>Interested</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>
                                            <img src="{{ asset('imgs/image.png') }}" class="h-7" alt="">
                                        </td>
                                        <td>
                                            @if (in_array($post->status->name, ['taken', 'deleted', 'archived']))
                                                <a class="text-decoration-none" href="{{ route('admin.posts.show', ['post' => $post->id]) }}">{{ $post->title }}</a>
                                            @else
                                                <a class="text-decoration-none" href="{{ route('admin.posts.edit', ['post' => $post->id]) }}">{{ $post->title }}</a>
                                            @endif
                                        </td>
                                        <td></td>
                                        <td><span class="tag tag-rounded {{ $post->status->class }}">{{ $post->status->name }}</span></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert-alert info">Posts empty</div>
                @endif
            </div>
        </div>
    </div>
@endsection
