<div class="header py-4">
    <div class="container">
      <div class="d-flex">
        <a class="header-brand font-family-quicksand" href="{{route('dashboard')}}">
          <img src="{{asset('imgs/logo.png')}}" class="header-brand-img" alt="freecycler logo">
          freecycler
        </a>
        <div class="d-flex order-lg-2 ml-auto">
            <div class="nav-item d-none d-md-flex">
                <a href="#" class="btn btn-sm btn-outline-primary mr-4"><i class="fe fe-edit"></i> Create a post</a>
            </div>
            <div class="dropdown d-none d-md-flex">
                <a class="nav-link icon" data-toggle="dropdown">
                  <i class="fe fe-bell"></i>
                  <span class="nav-unread"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url({{asset('demo/faces/male/41.jpg')}})"></span>
                    <div>
                      <strong>Nathan</strong> pushed new commit: Fix page load performance issue.
                      <div class="small text-muted">10 minutes ago</div>
                    </div>
                  </a>
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url({{asset('demo/faces/female/1.jpg')}})"></span>
                    <div>
                      <strong>Alice</strong> started new task: Tabler UI design.
                      <div class="small text-muted">1 hour ago</div>
                    </div>
                  </a>
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url({{asset('demo/faces/female/18.jpg')}})"></span>
                    <div>
                      <strong>Rose</strong> deployed new version of NodeJS REST Api V3
                      <div class="small text-muted">2 hours ago</div>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item text-center">Mark all as read</a>
                </div>
              </div>
              <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                  <span class="avatar" style="background-image: url({{asset('demo/faces/female/25.jpg')}})"></span>
                  <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">{{ auth()->user()->fullname }}</span>
                    <small class="text-muted d-block mt-1">Premium</small>
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a class="dropdown-item" href="#">
                    <i class="dropdown-icon fe fe-user"></i> Profile
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="dropdown-icon fe fe-settings"></i> Settings
                  </a>
                  <a class="dropdown-item" href="#">
                    <span class="float-right"><span class="badge badge-primary">6</span></span>
                    <i class="dropdown-icon fe fe-mail"></i> Inbox
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="dropdown-icon fe fe-send"></i> Message
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">
                    <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                  </a>
                  <a class="dropdown-item" href="{{route('logout')}}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="dropdown-icon fe fe-log-out"></i> Sign out
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                </div>
              </div>
        </div>
        <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
          <span class="header-toggler-icon"></span>
        </a>
      </div>
    </div>
  </div>
  <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-3 ml-auto">
          <form class="input-icon my-3 my-lg-0">
            <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
            <div class="input-icon-addon">
              <i class="fe fe-search"></i>
            </div>
          </form>
        </div>
        <div class="col-lg order-lg-first">
          <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
            <li class="nav-item">
              <a href="{{route('dashboard')}}" class="nav-link @if(Route::current()->getName() == 'dashboard')active @endif"><i class="fe fe-home"></i> Dashboard</a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.posts.index')}}" class="nav-link @if(request()->is('admin/posts*'))active @endif" ><i class="fe fe-edit-3"></i> Posts</a>
            </li>
            {{-- <li class="nav-item">
                <a href="{{route('admin.invoices.index')}}" class="nav-link @if(request()->is('admin/invoices*'))active @endif" ><i class="fe fe-hash"></i> Invoices</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.templates.index')}}" class="nav-link @if(request()->is('admin/templates*'))active @endif" ><i class="fe fe-layout"></i> Templates</a>
            </li>
            <li class="nav-item dropdown">
              <a href="{{route('admin.communications.index')}}" class="nav-link @if(request()->is('admin/communications*'))active @endif"><i class="fe fe-file"></i> Communications</a>
            </li> --}}
          </ul>
        </div>
      </div>
    </div>
  </div>
