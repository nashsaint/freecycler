<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('imgs/favicon/favicon.ico')}}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('imgs/favicon/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('imgs/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ASSET('imgs/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('imgs/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>Freecycler</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />
    <link href="{{asset('css/main.css')}}" rel="stylesheet" />
    <script src="{{asset('js/app.js')}}" defer></script>
    <script src="{{asset('js/chat.js')}}" defer></script>
    {{-- <script src="{{asset('js/main.js')}}" defer></script> --}}

  </head>
  <body class="">
    <div class="page" id="app">
        <div class="flex-fill">
            @include('layouts.partials.user-header')
            <div class="my-3 my-md-5">
                @yield('content')
            </div>
        </div>

        <chat :authenticated="{{ $authenticated }}"></chat>

    </div>

    @yield('scripts')

  </body>
</html>
