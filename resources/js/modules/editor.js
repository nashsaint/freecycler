import Quill from "quill";
import { QuillToolbarDropDown } from '../vendors/dynamic-quill-tools';

export class Editor {
    /**
     * initialize editor
     *
     * @param {string} selector
     * @param {boolean} signature
     */
    constructor(selector, minimal = false) {
        let container = document.getElementById(selector),
            toolbars = []

        if (minimal) {
            toolbars = []
        } else {
            toolbars = [
                [{ header: [3, 4, 5, 6, false] }],
                ["bold", "italic", "underline", "strike"],
                [{ list: "ordered" }, { list: "bullet" }],
                [{ indent: "-1" }, { indent: "+1" }],
                [{ align: [] }],
            ]
        }

        let quill = new Quill(container, {
            theme: "snow",
            modules: {
                toolbar: {
                    container: toolbars,
                },
            },
        });

        this.selector = selector
        this.container = container
        this.quill = quill
    }

    getHTML() {
        return this.quill.root.innerHTML;
    }

    setHTML(html) {
        this.quill.root.innerHTML = html
    }
}
