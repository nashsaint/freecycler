import Choices from "choices.js"

export class Choice {
    constructor(selector) {
        let el = document.querySelector(selector)
        const choice = new Choices(el)
    }
}
