/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Swal from 'sweetalert2'
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
window.Swal = Swal
window.Toast = Toast

import dayjs from 'dayjs';
window.dayjs = dayjs;

// quill editor
import { Editor } from './modules/editor'
window.Editor = Editor

import { Choice } from './modules/choice'
window.Choice = Choice

import Vuex from 'vuex'
Vue.use(Vuex)

import Post from './store/modules/posts/post'
import Posts from './store/modules/posts/posts'
import Message from './store/modules/message'
const store = new Vuex.Store({
    modules: {
        Post,
        Posts,
        Message
    }
})

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Echo.private('chat')
// .listen('.MessageCreated', (e) => {
//     alert('cc')
//     this.messages.push({
//         message: e.message.content,
//         user: e.user
//     });
// });

const app = new Vue({
    el: '#app',
    store
});
