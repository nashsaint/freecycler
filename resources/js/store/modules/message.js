import routes from "../routes"

export default {
    state: {
        messages: [],
        message: {
            thread_id: 0,
            from_id: 0,
            content: ''
        },

        threads: [],
        thread: {}
    },

    getters: {
        getThread(state) {
            return state.threads
        },
        getThreads(state) {
            return state.threads
        },
        getMessage(state) {
            return state.message
        },
        getMessages(state) {
            return state.messages
        }
    },

    actions: {
        async getThreads({commit, state}) {
            try {
                let route = routes.getThreadsRoute('index')
                const result = await axios.get(route)
                    .then(R => {
                        commit('setThreads', R.data)
                        return true
                    })
                    .catch(E => {
                        return false
                    })
                return result
            } catch (error) {
                return false
            }
        },

        async sendMessage({commit, state}, message) {
            try {
                let route = routes.getMessagesRoute('store')
                const result = await axios.post(route, state.message)
                    .then(R => {
                        // state.messages.push(R.data)
                        return R.data
                    })
                    .catch(E => {
                        return false
                    })
                return result
            } catch (error) {
                return false
            }
        }
    },

    mutations: {
        setThreads(state, threads) {
            Object.keys(threads).forEach(k => {
                state.threads.push(threads[k])
            })
            state.threads = threads
        }
    }
}
