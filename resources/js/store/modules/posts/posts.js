import routes from "../../routes"

export default {
    state: {
        posts: [],
        page: 0,
        lastPage: 0
    },

    getters: {
        getPosts(state) {
            return state.posts
        },
        getCurrentPage(state) {
            return state.page
        },
        getLastPage(state) {
            return state.lastPage
        }
    },

    actions: {
        async getPosts({commit, state}) {
            try {
                commit('incrementPage')
                const result = await axios.get(`/posts?page=${state.page}`).then(R => {
                        commit('addPosts', R.data.data)
                        commit('setLastPage', R.data.last_page)
                        console.log(state.lastPage)
                    }).catch(E => {
                        return false
                    })
                return result

            } catch (error) {
                return false
            }
        }
    },

    mutations: {
        addPosts(state, posts) {
            posts.forEach(p => {
                state.posts.push(p)
            })
        },
        incrementPage(state) {
            state.page++
        },
        setLastPage(state, page) {
            state.lastPage = page
        }
    }
}
