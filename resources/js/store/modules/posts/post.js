import routes from "../../routes"

export default {
    state: {
        post: {
            id: 0,
            status_id: 1,
            title: '',
            description: '',
            images: [],
            threads: []
        }
    },

    getters: {
        getPost(state) {
            return state.post
        },
        getPostsRoute(state) {
            return (name, id) => {
                return routes.getPostsRoute(name, id)
            }
        }
    },

    actions: {
        async uploadImages({commit, state}, data) {
            try {
                const result = await axios.post(`/admin/post/images`, data,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(R => {
                    if (R.data.error) {
                        return R.data
                    }

                    state.post.images = R.data.images
                    return true
                }).catch(E => {
                    console.log('error', E.response.data)
                    return false
                })

                return result
            } catch (error) {
                return false
            }
        },

        async deleteImage({commit, state}, id) {
            try {
                const result = await axios.delete(`/admin/post/image/${id}`)
                    .then(R => {
                        Toast.fire({
                            icon: 'success',
                            title: 'Image successfully deleted'
                        })
                        let index = state.post.images.findIndex(i => i.id == id)
                        console.log(index)
                        state.post.images.splice(index, 1)
                        return {deleted: true}
                    })
                    .catch(E => {
                        Toast.fire({
                            icon: 'error',
                            title: 'There was an error deleting an image'
                        })
                        return {error: true}
                    })

                return result
            } catch (error) {
                Toast.fire({
                    icon: 'error',
                    title: 'There was an error deleting an image'
                })
            }
        },

        async updatePost({commit, state}) {
            try {
                let route = routes.getPostsRoute('update', state.post.id)
                const result = await axios.put(route, state.post)
                    .then(R => {
                        setTimeout(() => {
                            Toast.fire({
                                icon: 'success',
                                title: 'Post has been successfully updated'
                            })
                            return true
                        }, 200);
                    })
                    .catch(E => {
                        console.log(E.response.data)
                        Toast.fire({
                            icon: 'error',
                            title: 'There was a problem while updating the post'
                        })
                        return false
                    })
                return result

            } catch (error) {
                Toast.fire({
                    icon: 'error',
                    title: 'There was a problem while updating the post'
                })
            }
        },

        async savePost({commit, state}, post) {
            try {
                let route = routes.getPostsRoute('store')
                const result =  await axios.post(route, post)
                    .then(R => {
                        Toast.fire({
                            icon: 'success',
                            title: 'Post successfully saved'
                        })
                        return {route: routes.getPostsRoute('index')}
                    })
                    .catch(E => {
                        Toast.fire({
                            icon: 'error',
                            title: 'There was a problem while saving the post'
                        })
                        return false
                    })
                return result
            } catch (error) {
                Toast.fire({
                    icon: 'error',
                    title: 'There was a problem while saving the post'
                })
            }
        }
    },

    mutations: {
        setPost(state, post) {
            Object.keys(post).forEach(k => {
                if (state.post.hasOwnProperty(k)) {
                    state.post[k] = post[k]
                }
            })
        }
    }
}
