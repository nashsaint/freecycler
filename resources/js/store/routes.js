export default {
    getPostsRoute(name = '', id = null) {
        return this.getGenericRoutes('posts', name, id)
    },

    getThreadsRoute(name = '', id = null) {
        return this.getGenericRoutes('threads', name, id)
    },

    getMessagesRoute(name = '', id = null) {
        return this.getGenericRoutes('messages', name, id)
    },

    getGenericRoutes(type, name, id) {
        let routes = {
            index: `/admin/${type}`,
            store: `/admin/${type}`,
            update: `/admin/${type}/${id}`,
            delete: `/admin/${type}/${id}`
        }

        if (routes.hasOwnProperty(name)) {
            return routes[name]
        }

        return routes
    }
}
