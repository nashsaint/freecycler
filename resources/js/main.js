import Quill from "quill";
import { QuillToolbarDropDown } from './vendors/dynamic-quill-tools';

let editor = document.getElementById("editor");
if (editor) {
    attachDropdown(editor)
}

var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        $(mutation.addedNodes).find("#editor").each(function () {
            let editor = document.getElementById("editor");
            attachDropdown(editor)
        });
    });
});
observer.observe(document.body, { childList: true, subtree: true });
function attachDropdown(container) {
    const quill = new Quill(container, {
        theme: "snow",
        modules: {
            toolbar: {
                container: [
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                    [{ font: [] }],
                    ["bold", "italic", "underline", "strike"],
                    [{ list: "ordered" }, { list: "bullet" }],
                    [{ indent: "-1" }, { indent: "+1" }],
                    [{ align: [] }],
                ],
            },
        },
    });
    const dropDownItems = {
        "Cient Name": "{{CLIENT NAME}}",
        "Client Fullname": "{{CLIENT FULLNAME}}",
        "Client Address": "{{CLIENT ADDRESS}}",
        "Client Signature": "{{CLIENT SIGNATURE}}",
    };
    const myDropDown = new QuillToolbarDropDown({
        label: "Form Tags",
        rememberSelection: false,
    });

    myDropDown.setItems(dropDownItems);
    myDropDown.onSelect = function (label, value, quill) {
        const { index, length } = quill.selection.savedRange;
        quill.deleteText(index, length);
        quill.insertText(index, value);
        quill.setSelection(index + value.length);
    };
    myDropDown.attach(quill);
}
