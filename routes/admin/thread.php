<?php

use App\Http\Controllers\Admin\Messages\ThreadController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
    Route::resource('threads', ThreadController::class)
        ->names([
            'index' => 'admin.threads.index',
            'create' => 'admin.threads.create',
            'show' => 'admin.threads.show',
            'store' => 'admin.threads.store',
            'edit' => 'admin.threads.edit',
            'update' => 'admin.threads.update',
            'destroy' => 'admin.threads.destroy'
        ]);
});

