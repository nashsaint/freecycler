<?php

use App\Http\Controllers\Admin\Post\PostController;
use App\Http\Controllers\Admin\Post\PostImageController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
    Route::resource('posts', PostController::class)
        ->names([
            'index' => 'admin.posts.index',
            'create' => 'admin.posts.create',
            'show' => 'admin.posts.show',
            'store' => 'admin.posts.store',
            'edit' => 'admin.posts.edit',
            'update' => 'admin.posts.update',
            'destroy' => 'admin.posts.destroy'
        ]);

    Route::post('post/images', [PostImageController::class, 'upload'])->name('upload');
    Route::delete('post/image/{image}', [PostImageController::class, 'destroy']);
});
