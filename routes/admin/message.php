<?php

use App\Http\Controllers\Admin\Messages\MessageController;
use Illuminate\Support\Facades\Route;

Route::get('admin/message/chat', [MessageController::class, 'chat']);
Route::group(['prefix' => 'admin'], function () {
    Route::resource('messages', MessageController::class)
        ->names([
            'index' => 'admin.messages.index',
            'create' => 'admin.messages.create',
            'show' => 'admin.messages.show',
            'store' => 'admin.messages.store',
            'edit' => 'admin.messages.edit',
            'update' => 'admin.messages.update',
            'destroy' => 'admin.messages.destroy'
        ]);
});

