<?php

namespace App\Http\Controllers\Admin\Messages;

use App\Http\Controllers\Controller;
use App\Models\Messages\Thread;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        $threads = Thread::with('messages')
            ->where('from_id', $user_id)
            ->orWhere('to_id', $user_id)
            ->get();

            if (request()->ajax()) {
                return response($threads);
            }
    }
}
