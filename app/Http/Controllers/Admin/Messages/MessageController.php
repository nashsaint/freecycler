<?php

namespace App\Http\Controllers\Admin\Messages;

use App\Events\Message\MessageCreated;
use App\Http\Controllers\Controller;
use App\Models\Messages\Message;
use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        $messages = Message::where('from_id', $user_id)
            ->orWhere('to_id', $user_id)
            ->get();

        if (request()->ajax()) {
            return response($messages);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = auth()->user();

        data_set($data, 'from_id', $user->id);

        $message = Message::create($data);

        MessageCreated::dispatch($message, $user);

        // broadcast(new MessageCreated($message))->toOthers();

        return response($message);
    }

    public function chat()
    {
        $message = Message::create([
            'thread_id' => 1,
            'from_id' => 1,
            'content' => 'test'
        ]);

        MessageCreated::dispatch($message);

        return 'x';
    }

}
