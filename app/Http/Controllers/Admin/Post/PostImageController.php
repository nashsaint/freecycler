<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Models\Posts\Post;
use App\Models\Posts\PostImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class PostImageController extends Controller
{
    public function upload(Request $request)
    {

        $validator = $this->validateImages($request->all());

        if ($validator->fails()) {
            return response(['error' => $validator->errors()->all()]);
        }

        $filename = sprintf("%s/%s.jpg", auth()->user()->id, uniqid());
        $path = "posts/" . $filename;

        $post = Post::find($request->post_id);
        $images = $request->file('images');
        foreach ($images as $image) {
            $newImage = $this->makeImage($image);
            Storage::put($path, $newImage['content']);

            PostImage::create([
                'post_id' => $post->id,
                'filename' => $filename,
                'size' => $newImage['size'],
                'mime' => $newImage['mime']
            ]);
        }

        return response($post->load('images'));
    }

    public function validateImages($request)
    {
        $messages = [
            'mimes' => 'The image must be of type: jpeg, jpg or png',
            'max' => 'The image cannot be greater than 2048 kilobytes'
        ];

        $validator = Validator::make($request, [
            'images.*' => 'required|mimes:jpeg,png,jpg,gif|max:2048'
        ], $messages);

        return $validator;
    }

    public function makeImage($image)
    {
        $img = Image::make($image->path());
        $img->resize(980, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return [
            'content' => $img->stream('jpg'),
            'size' => $img->filesize(),
            'mime' => $img->mime
        ];
    }

    public function destroy(PostImage $image)
    {
        Storage::delete('posts/' . $image->filename);
        $image->delete();

        return response(['deleted' => true]);
    }
}
