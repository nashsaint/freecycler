<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Auth\Authenticatable;

class ShareAuthenticatedUser
{
    /**
     * @var \Illuminate\Contracts\View\Factory
     */
    protected Factory $factory;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected Authenticatable $user;

    /**
     * Create a new Share Authenticated User instance.
     *
     * @param  \Illuminate\Contracts\View\Factory  $factory
     * @param  \Illuminate\Contracts\Auth\Authenticatable|null  $user
     */
    public function __construct(Factory $factory, Authenticatable $user = null)
    {
        $this->factory = $factory;
        if ($user) {
            $this->user = $user;
        }
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->factory->share('authenticated', $this->user ?? null);

        return $next($request);
    }
}
