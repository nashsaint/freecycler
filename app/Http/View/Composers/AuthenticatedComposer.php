<?php

namespace App\Http\View\Composers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class AuthenticatedComposer
{
    protected $user;

    public function __construct(Authenticatable $user = null)
    {
        $this->user = $user;
    }

    public function compose(View $view)
    {
        $view->with('authenticated', $this->user);
    }
}
