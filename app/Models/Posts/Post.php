<?php

namespace App\Models\Posts;

use App\Events\Post\PostCreating;
use App\Models\Messages\Thread;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'status_id', 'title', 'description'];

    public function status()
    {
        return $this->belongsTo(PostStatus::class);
    }

    public function images()
    {
        return $this->hasMany(PostImage::class);
    }

    public function messages()
    {
        return $this->belongsToMany(Message::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    protected $dispatchesEvents = [
        'creating' => PostCreating::class
    ];
}
