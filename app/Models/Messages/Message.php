<?php

namespace App\Models\Messages;

use App\Events\Message\MessageCreated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = ['thread_id', 'from_id', 'to_id', 'content', 'read_at'];

    // protected $dispatchesEvents = [
    //     'created' => MessageCreated::class
    // ];
}
